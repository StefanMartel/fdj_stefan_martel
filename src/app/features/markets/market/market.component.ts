import {Component, Input, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-market',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './market.component.html',
  styleUrls: ['./market.component.scss']
})
export class MarketComponent implements OnInit {

  @Input() market: any;

  valueToDisplay = '';

  constructor() { }

  ngOnInit(): void {
    this.valueToDisplay = this.market.key.substring(0,1) === 'l' ? this.market.value.desc : this.market.value.parent
    console.log(this.valueToDisplay)
  }

}
