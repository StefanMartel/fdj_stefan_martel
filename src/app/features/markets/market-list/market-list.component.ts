import {Component, Input, OnInit} from '@angular/core';
import {MarketComponent} from "../market/market.component";
import {MarketService} from "../../../shared/market.service";
import {CommonModule} from "@angular/common";

@Component({
  selector: 'app-market-list',
  standalone: true,
  imports: [MarketComponent, CommonModule],
  templateUrl: './market-list.component.html',
  styleUrls: ['./market-list.component.scss']
})
export class MarketListComponent implements OnInit {

  markets: any;

  constructor(
    private marketService: MarketService
  ) { }

  ngOnInit(): void {
    this.marketService.getMarkets().subscribe(
      data => {
        let tmpData = data.items
        this.markets = Object.keys(tmpData).map(key => ({key, value: tmpData[key]}))
        console.log (this.markets)
      }
    )
  }

}
