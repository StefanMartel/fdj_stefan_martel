import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Injectable} from "@angular/core";

export class HttpService {
  private headers: HttpHeaders;

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({ 'Content-Type': 'application/json' });
  }

  httpCallGet(pathToCall: string): Observable<any> {
    console.log('ENV', environment.backEndUrl + pathToCall)
    return this.http
      .get(environment.backEndUrl + pathToCall, { headers: this.headers, observe: 'response' })
      .pipe(map(res => res.body));
  }

}
