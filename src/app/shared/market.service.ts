import {HttpService} from "./http.service";
import {Observable, of} from "rxjs";
import {environment} from "../../environments/environment";
import {Injectable} from "@angular/core";
import {MARKETS} from "./markets";

@Injectable({
  providedIn: "root"
})
export class MarketService {
  constructor(
    private httpService: HttpService
  ){}

  getMarkets(): Observable<any>{
    return of(MARKETS)
    // return this.httpService.httpCallGet(environment.marketList)
  }
}
