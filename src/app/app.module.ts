import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {MarketListComponent} from "./features/markets/market-list/market-list.component";
import {MarketComponent} from "./features/markets/market/market.component";
import {HttpService} from "./shared/http.service";
import {HttpClient, HttpClientModule} from "@angular/common/http";

export function httpService(http: HttpClient) {
  return new HttpService(http);
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    MarketListComponent,
    MarketComponent
  ],
  providers: [
    { provide: HttpService, useFactory: httpService, deps: [HttpClient] },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
